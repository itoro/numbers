type show =
  | Help
  | Solution
;;

type action =
  | Debug_keys
  | New
  | Refresh
  | Rotation of Cube.rotation
  | Save
  | Show of show
  | Shuffle
  | Switch of int
;;

type command =
  | Action of action
  | Quit
;;

module Int =
  struct
    type t = int;;
    let compare = compare;;
  end
;;

module Int_map = Map.Make (Int);;

type state =
  {
    cubes : Cube.t Int_map.t;
    current : int;
  }
;;

let get_cube = function
  | {
      cubes = cubes;
      current = current;
    } -> Int_map.find current cubes
;;

let with_cube f = function
  | {
      cubes = cubes;
      current = current;
    } as state ->
    let cube = Int_map.find current cubes in
    let cube = f cube in
    let cubes = Int_map.add current cube cubes in
    { state with cubes = cubes; }
;;

let with_open_in file f =
  let ic = open_in_bin file in
  let r =
    try
      f ic
    with
    | x ->
      close_in ic;
      raise x in
  close_in ic;
  r
;;

let with_open_out file f =
  let oc = open_out_bin file in
  let r =
    try
      f oc
    with
    | x ->
      close_out oc;
      Sys.remove file;
      raise x in
  close_out oc;
  r
;;

let save config (state : state) =
  with_open_out
    config
    (fun oc -> Marshal.to_channel oc state [])
;;

let load config : state option =
  if Sys.file_exists config then
    Some (with_open_in config (fun ic -> Marshal.from_channel ic))
  else
    None
;;

let help =
  [
    "q      Quit";
    "^@     Debug keys";
    "^H, ?  Show help";
    "^I     Show solution";
    "^L     Refresh";
    "n      Generate new cube";
    "r      Shuffle cube";
    "s      Save";
    "3..0   Switch to cube of specified size";
    "y      Rotation y";
    "u      Rotation u";
    "i      Rotation i";
    "o      Rotation o";
    "h      Rotation h";
    "j      Rotation j";
    "k      Rotation k";
    "l      Rotation l";
    "-- Press ^H to continue --"
  ]
;;

let command_of_key = function
  (* quit *)
  | 'q' -> Some (Quit)
  (* action *)
  | '\000' -> Some (Action Debug_keys)
  | '\008' | '?' -> Some (Action (Show Help))
  | '\009' -> Some (Action (Show Solution))
  | '\012' -> Some (Action Refresh)
  | 'n' -> Some (Action New)
  | 'r' -> Some (Action Shuffle)
  | 's' -> Some (Action Save)
  | d when '3' <= d && d <= '9' ->
    Some (Action (Switch (int_of_char d - int_of_char '0')))
  | '0' -> Some (Action (Switch 10))
  (* rotation *)
  | 'y' -> Some (Action (Rotation Cube.Rotation_y))
  | 'u' -> Some (Action (Rotation Cube.Rotation_u))
  | 'i' -> Some (Action (Rotation Cube.Rotation_i))
  | 'o' -> Some (Action (Rotation Cube.Rotation_o))
  | 'h' -> Some (Action (Rotation Cube.Rotation_h))
  | 'j' -> Some (Action (Rotation Cube.Rotation_j))
  | 'k' -> Some (Action (Rotation Cube.Rotation_k))
  | 'l' -> Some (Action (Rotation Cube.Rotation_l))
  | _ -> None
;;

let rec read_command () =
  match command_of_key (Term.read_key ()) with
  | None -> read_command ()
  | Some command -> command
;;

let print_state state =
  let cube = get_cube state in
  Cube.print cube
;;

let refresh state =
  Term.reset ();
  print_state state
;;

let draw state =
  Term.home ();
  print_state state
;;

let debug_keys () =
  let rec loop () =
    let k = Term.read_key () in
    Printf.eprintf "%C %d" k (int_of_char k);
    prerr_newline ();
    if k <> '\000' then
      loop () in
  prerr_endline "debug";
  loop ()
;;

let show state s =
  Term.reset ();
  match s with
  | Help -> List.iter print_endline help
  | Solution ->
    Cube.print_solution (get_cube state);
    print_endline "-- Press TAB to continue --"
;;

let rec loop config state =
  match read_command () with
  | Action action -> loop_action config state action
  | Quit -> save config state
and loop_action config state = function
  | Debug_keys ->
    debug_keys ();
    loop config state
  | New -> loop_new config state
  | Refresh -> loop_refresh config state
  | Rotation rotation -> loop_rotation config rotation state
  | Save -> loop_save config state
  | Show s -> loop_show config s state
  | Shuffle -> loop_shuffle config state
  | Switch i -> loop_switch config i state
and loop_new config = function
  | {
      cubes = _;
      current = current;
    } as state ->
    let state = with_cube (fun _ -> Cube.mk_cube current) state in
    refresh state;
    loop config state
and loop_refresh config state =
  refresh state;
  loop config state
and loop_rotation config rotation state =
  let f cube = Cube.rotate cube rotation in
  let state = with_cube f state in
  draw state;
  loop config state
and loop_save config state =
  save config state;
  refresh state;
  loop config state
and loop_show config s state =
  show state s;
  let rec wait () =
    match read_command () with
    | Action action ->
      begin
        match action with
        | Refresh ->
          show state s;
          wait ()
        | Show ns ->
          if s = ns then
            begin
              refresh state;
              loop config state
            end
          else
            loop_show config ns state
        | Debug_keys ->
          debug_keys ();
          wait ()
        | Rotation r -> loop_rotation config r state
        | Switch i -> loop_switch config i state
        | New -> loop_new config state
        | Save ->
          save config state;
          wait ()
        | Shuffle -> wait ()
      end
    | Quit -> save config state in
  wait ()
and loop_shuffle config state =
  let state = with_cube Cube.shuffle state in
  refresh state;
  loop config state
and loop_switch config i = function
  | {
      cubes = cubes;
      current = _;
    } ->
    let cubes =
      if not (Int_map.mem i cubes) then
        Int_map.add i (Cube.mk_cube i) cubes
      else
        cubes in
    let state =
      {
        cubes = cubes;
        current = i;
      } in
    refresh state;
    loop config state
;;

let mk_init_state () =
  let current = 3 in
  let cubes =
    Int_map.add current (Cube.mk_cube current) Int_map.empty in
  {
    cubes = cubes;
    current  = current;
  }
;;

let version = 1;;

let get_set default =
  let r = ref default in
  let get () = !r in
  let set x = r := x in
  get, set
;;

let get_save_file, set_save_file = get_set None;;

let get_show_version, set_show_version = get_set false;;

let get_save_file () =
  match get_save_file () with
  | None ->
    let home = Sys.getenv "HOME" in
    Filename.concat home ".numbers"
  | Some f -> f
;;

let get_with_term_method, set_with_term_method =
  get_set Config.default_with_term_method
;;

let speclist =
  let set_save_file = Arg.String (fun s -> set_save_file (Some s)) in
  let set_show_version = Arg.Unit (fun () -> set_show_version true) in
  let set_term_method =
    let set_term_method = function
      | "termios" -> set_with_term_method Term.Termios
      | "stty" -> set_with_term_method Term.Stty
      | _ -> assert false in
    Arg.Symbol ([ "termios"; "stty"; ], set_term_method) in
  Arg.align
    [
      "-f", set_save_file, "<file> Set save file";
      "--term", set_term_method, "<method> Set term method";
      "--version", set_show_version, " Show version";
    ]
;;

let anon_fun s = raise (Arg.Bad (Printf.sprintf "unknown arg: %S" s));;

let usage_msg =
  Printf.sprintf "usage: %s [options]" (Filename.basename Sys.argv.(0))
;;

let main_loop () =
  Arg.parse speclist anon_fun usage_msg;
  Random.self_init ();
  let config =
    let save_file = get_save_file () in
    save_file in
  let first_time, state =
    match load config with
    | None -> true, mk_init_state ()
    | Some state -> false, state in
  Term.with_term
    (get_with_term_method ())
    (fun () ->
      if first_time then
        loop_show config Help state
      else
        begin
          refresh state;
          loop config state
        end);
  Term.reset ()
;;

let main () =
  Arg.parse speclist anon_fun usage_msg;
  if get_show_version () then
    print_endline
      (Printf.sprintf "%s v%d" (Filename.basename Sys.argv.(0)) version)
  else
    main_loop ()
;;

let () = main ();;
