let command cmd =
  match Sys.command cmd with
  | 0 -> ()
  | _ -> failwith (Printf.sprintf "command failed: %s" cmd)
;;

let open_term () = command "stty -cooked -echo";;

let close_term () = command "stty cooked echo";;
