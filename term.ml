type with_term_method =
  | Stty
  | Termios
;;

let get_open_close = function
  | Stty -> Term_stty.open_term, Term_stty.close_term
  | Termios -> Term_termios.open_term, Term_termios.close_term
;;

let with_term term_method f =
  let open_term, close_term = get_open_close term_method in
  open_term ();
  let r =
    try
      f ()
    with
    | x ->
      close_term ();
      raise x in
  close_term ();
  r
;;

let read_key () = input_char stdin;;

let send s =
  print_string s;
  flush stdout
;;

let reset () = send "\027c";;

let home () = send "\027[H";;
