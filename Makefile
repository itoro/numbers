include .config.mk

OCAMLUSERFLAGS=
OCAMLCUSERFLAGS=$(OCAMLUSERFLAGS)
OCAMLOPTUSERFLAGS=$(OCAMLUSERFLAGS)

OCAMLDEVFLAGS=-w A -warn-error A -safe-string -strict-sequence
OCAMLCDEVFLAGS=-annot $(OCAMLDEVFLAGS)
OCAMLOPTDEVFLAGS=$(OCAMLDEVFLAGS)

OCAMLCFLAGS=$(OCAMLC$(MODE)FLAGS)
OCAMLOPTFLAGS=$(OCAMLOPT$(MODE)FLAGS)
OCAMLC=ocamlc $(OCAMLCFLAGS)
OCAMLOPT=ocamlopt $(OCAMLOPTFLAGS)
OCAMLDEP=ocamldep

RM=rm -f
CHMOD=chmod

include Objs.mk

.PHONY: all clean depend

all: byt bin

clean:
	$(RM) $(EXES) $(OBJS) $(INTERFACES) $(SPURIOUSFILES)

depend:
	@$(RM) .depend.mk
	$(OCAMLDEP) $(CAMLFILES) >.depend.mk
	@$(CHMOD) 0000 .depend.mk
	@$(CHMOD) +rw .depend.mk
	@$(CHMOD) -w .depend.mk

.PHONY: interfaces byt bytobjs bin binobjs

interfaces: $(INTERFACES)

byt: $(BYTEXE)

bytobjs: $(BYTOBJS)

$(BYTEXE): $(BYTOBJS)
	$(OCAMLC) -o $(BYTEXE) $(BYTLIBS) $(BYTOBJS)

bin: $(BINEXE)

binobjs: $(BINOBJS)

$(BINEXE): $(BINOBJS)
	$(OCAMLOPT) -o $(BINEXE) $(BINLIBS) $(BINOBJS)

.SUFFIXES: .cmi .cmo .cmx .ml .mli

.ml.cmo:
	$(OCAMLC) -c $<

.ml.cmx:
	$(OCAMLOPT) -c $<

.mli.cmi:
	$(OCAMLC) -c $<

include .depend.mk
