(** Module providing all cube related types and functions. *)

type cube
(** The abstract data type representing a cube. *)
;;

type t = cube
(** An alias. *)
;;

type rotation =
  | Rotation_y
  | Rotation_u
  | Rotation_i
  | Rotation_o
  | Rotation_h
  | Rotation_j
  | Rotation_k
  | Rotation_l
(** Different possible rotations. *)
;;

val rotate : cube -> rotation -> cube
(** [rotation cube rotation] applies [rotation] to [cube]. *)
;;

val shuffle : cube -> cube
(** [shuffle cube] shuffles [cube]. *)
;;

val mk_cube : int -> cube
(** [mk_cube size] makes a new shuffled cube of size [size]. *)
;;

val print : cube -> unit
(** [print cube] prints [cube]. *)
;;

val print_solution : cube -> unit
(** [print_solution cube] prints solved cube for [cube]. *)
;;

val is_solved : cube -> bool
(** [is_solved cube] checks if [cube] is solved. *)
;;
