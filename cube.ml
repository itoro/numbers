module Int_pair =
  struct
    type t = int * int;;
    let compare = compare;;
  end
;;

module Int_pair_map = Map.Make (Int_pair);;

type raw_cube =
  {
    cell_size : int;
    cells : string Int_pair_map.t Int_pair_map.t;
    size : int;
  }
;;

type cube =
  {
    current : raw_cube;
    solution : raw_cube;
  }
;;

type t = cube;;

type rotation =
  | Rotation_y
  | Rotation_u
  | Rotation_i
  | Rotation_o
  | Rotation_h
  | Rotation_j
  | Rotation_k
  | Rotation_l
;;

let extract_rotation = function
  | Rotation_y -> 0, 0, 0
  | Rotation_u -> 0, 0, 1
  | Rotation_i -> 0, 1, 0
  | Rotation_o -> 0, 1, 1
  | Rotation_h -> 1, 0, 0
  | Rotation_j -> 1, 0, 1
  | Rotation_k -> 1, 1, 0
  | Rotation_l -> 1, 1, 1
;;

let rotate_int_map f trans size map s =
  let trans r c =
    let r, c = r / 2, c / 2 in
    trans r c in
  let rec loop r c accu =
    if r = 2 * size - 2 then
      accu
    else if c = 2 * size - 2 then
      loop (r + 2) 0 accu
    else
      let accu =
        (r, c, Int_pair_map.find (trans r c) map) :: accu in
      loop r (c + 2) accu in
  let accu = loop 0 0 [] in
  let trans r c =
    let r, c = r + size - 2, c + size - 2 in
    trans r c in
  let trans r c =
    let r, c =
      if s = 0 then
        - c, r
      else
        c, - r in
    trans r c in
  let trans r c =
    let r, c = r - size + 2, c - size + 2 in
    trans r c in
  let rec loop map = function
    | [] -> map
    | (r, c, x) :: accu ->
      let map = Int_pair_map.add (trans r c) (f x) map in
      loop map accu in
  loop map accu
;;

let rotate_raw_cube raw_cube rotation =
  match raw_cube with
  | {
      cell_size = _;
      cells = cells;
      size = size;
    } as cube ->
    let dr, dc, s = extract_rotation rotation in
    let trans r c = r, c in
    let rotate_cell cell =
      rotate_int_map (fun sub_cell -> sub_cell) trans 3 cell s in
    let trans r c =
      let r, c = r + dr, c + dc in
      trans r c in
    let rotate_cells cells =
      rotate_int_map rotate_cell trans size cells s in
    let cells = rotate_cells cells in
    { cube with cells = cells; }
;;

let rotate cube rotation =
  match cube with
  | {
      current = current;
      solution = _;
    } ->
    let current = rotate_raw_cube current rotation in
    { cube with current = current; }
;;

let shuffle cube =
  let rotations =
    [|
      Rotation_y;
      Rotation_u;
      Rotation_i;
      Rotation_o;
      Rotation_h;
      Rotation_j;
      Rotation_k;
      Rotation_l;
     |] in
  let rec loop cube = function
    | 0 -> cube
    | k ->
      let rotation = rotations.(Random.int (Array.length rotations)) in
      let cube = rotate cube rotation in
      loop cube (pred k) in
  loop cube (Random.int 500 + 500)
;;

let mk_cube size =
  let rec mk_list cell_size cells r c =
    if r = size then
      cell_size, cells
    else if c = size then
      mk_list cell_size cells (succ r) 0
    else
      let sub_cell_q = string_of_int (size * r + c + 1) in
      let sub_cell_w = "." in
      let sub_cell_a = "." in
      let sub_cell_s = "." in
      let sub_cells =
        [
          0, 0, sub_cell_q;
          0, 1, sub_cell_w;
          1, 0, sub_cell_a;
          1, 1, sub_cell_s;
        ] in
      let cell, cell_size =
        List.fold_left
          (fun (cell, cell_size) (i, j, sub_cell) ->
           Int_pair_map.add (i, j) sub_cell cell,
           max cell_size (String.length sub_cell))
          (Int_pair_map.empty, cell_size)
          sub_cells in
      let cells = Int_pair_map.add (r, c) cell cells in
      mk_list cell_size cells r (succ c) in
  let cell_size, cells = mk_list 0 Int_pair_map.empty 0 0 in
  let pad s =
    let w_pad = cell_size - String.length s in
    let r_pad = w_pad / 2 in
    let l_pad = w_pad - r_pad in
    Printf.sprintf
      "%s%s%s" (String.make l_pad ' ') s (String.make r_pad ' ') in
  let cells = Int_pair_map.map (Int_pair_map.map pad) cells in
  let raw_cube =
    {
      size = size;
      cell_size = cell_size;
      cells = cells;
    } in
  let cube =
    {
      current = raw_cube;
      solution = raw_cube;
    } in
  shuffle cube
;;

let print_endline s =
  Printf.fprintf stdout "%s\r\n" s;
  flush stdout
;;

let print_newline () =
  print_string "\r\n";
  flush stdout
;;

let print_raw_cube = function
  | {
      cell_size = cell_size;
      cells = cells;
      size = size;
    } ->
    let h_rule =
      String.init
        ((2 * cell_size + 2) * size + 1)
        (fun i ->
         if i mod (2 * cell_size + 2) == 0 then '+' else '-') in
    print_endline h_rule;
    let rec print_rows r =
      if r < size then
        let rec print_cols sub_r c =
          if c = size then
            if sub_r = 1 then
              begin
                print_newline ();
                print_endline h_rule;
                print_rows (succ r)
              end
            else
              begin
                print_newline ();
                print_char '|';
                print_cols (succ sub_r) 0
              end
          else
            begin
              let cell = Int_pair_map.find (r, c) cells in
              let sub_cell_l = Int_pair_map.find (sub_r, 0) cell in
              let sub_cell_r = Int_pair_map.find (sub_r, 1) cell in
              Printf.printf "%s %s|" sub_cell_l sub_cell_r;
              print_cols sub_r (succ c)
            end in
        print_char '|';
        print_cols 0 0 in
    print_rows 0
;;

let print = function
  | {
      current = current;
      solution = _;
    } -> print_raw_cube current
;;

let print_solution = function
  | {
      current = _;
      solution = solution;
    } -> print_raw_cube solution
;;

let is_solved = function
  | {
      current = current;
      solution = solution;
    } ->
    let compare_subcell (s1 : string) (s2 : string) = compare s1 s2 in
    let compare_cell = Int_pair_map.compare compare_subcell in
    let compare_cells = Int_pair_map.compare compare_cell in
    let compare_raw_cube raw_cube_1 raw_cube_2 =
      let cells_of_raw_cube = function
        | {
            cell_size = _;
            cells = cells;
            size = _;
          } -> cells in
      compare_cells
        (cells_of_raw_cube raw_cube_1) (cells_of_raw_cube raw_cube_2) in
    compare_raw_cube current solution = 0
;;
