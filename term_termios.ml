type status =
  {
    (* input modes *)
    c_ignbrk : bool;
    c_brkint : bool;
    c_ignpar : bool;
    c_parmrk : bool;
    c_inpck : bool;
    c_istrip : bool;
    c_inlcr : bool;
    c_igncr : bool;
    c_icrnl : bool;
    c_ixon : bool;
    c_ixoff : bool;
    (* Local modes: *)
    c_isig : bool;
    c_icanon : bool;
    c_noflsh : bool;
    c_echo : bool;
    c_echoe : bool;
    c_echok : bool;
    c_echonl : bool;
    (* Control characters: *)
    c_vmin : int;
    c_vtime : int;
  }
;;

let interactive =
  {
    (* input modes *)
    c_ignbrk = false;
    c_brkint = false;
    c_ignpar = false;
    c_parmrk = false;
    c_inpck = false;
    c_istrip = false;
    c_inlcr = false;
    c_igncr = false;
    c_icrnl = false;
    c_ixon = false;
    c_ixoff = false;
    (* Local modes: *)
    c_isig = false;
    c_icanon = false;
    c_noflsh = false;
    c_echo = false;
    c_echoe = false;
    c_echok = false;
    c_echonl = false;
    (* Control characters: *)
    c_vmin = 1;
    c_vtime = 0;
  }
;;

let to_status terminal_io =
  {
    (* input modes *)
    c_ignbrk = terminal_io.Unix.c_ignbrk;
    c_brkint = terminal_io.Unix.c_brkint;
    c_ignpar = terminal_io.Unix.c_ignpar;
    c_parmrk = terminal_io.Unix.c_parmrk;
    c_inpck = terminal_io.Unix.c_inpck;
    c_istrip = terminal_io.Unix.c_istrip;
    c_inlcr = terminal_io.Unix.c_inlcr;
    c_igncr = terminal_io.Unix.c_igncr;
    c_icrnl = terminal_io.Unix.c_icrnl;
    c_ixon = terminal_io.Unix.c_ixon;
    c_ixoff = terminal_io.Unix.c_ixoff;
    (* Local modes: *)
    c_isig = terminal_io.Unix.c_isig;
    c_icanon = terminal_io.Unix.c_icanon;
    c_noflsh = terminal_io.Unix.c_noflsh;
    c_echo = terminal_io.Unix.c_echo;
    c_echoe = terminal_io.Unix.c_echoe;
    c_echok = terminal_io.Unix.c_echok;
    c_echonl = terminal_io.Unix.c_echonl;
    (* Control characters: *)
    c_vmin = terminal_io.Unix.c_vmin;
    c_vtime = terminal_io.Unix.c_vtime;
  }
;;

let of_status terminal_io status =
  (* input modes *)
  terminal_io.Unix.c_ignbrk <- status.c_ignbrk;
  terminal_io.Unix.c_brkint <- status.c_brkint;
  terminal_io.Unix.c_ignpar <- status.c_ignpar;
  terminal_io.Unix.c_parmrk <- status.c_parmrk;
  terminal_io.Unix.c_inpck <- status.c_inpck;
  terminal_io.Unix.c_istrip <- status.c_istrip;
  terminal_io.Unix.c_inlcr <- status.c_inlcr;
  terminal_io.Unix.c_igncr <- status.c_igncr;
  terminal_io.Unix.c_icrnl <- status.c_icrnl;
  terminal_io.Unix.c_ixon <- status.c_ixon;
  terminal_io.Unix.c_ixoff <- status.c_ixoff;
  (* Local modes: *)
  terminal_io.Unix.c_isig <- status.c_isig;
  terminal_io.Unix.c_icanon <- status.c_icanon;
  terminal_io.Unix.c_noflsh <- status.c_noflsh;
  terminal_io.Unix.c_echo <- status.c_echo;
  terminal_io.Unix.c_echoe <- status.c_echoe;
  terminal_io.Unix.c_echok <- status.c_echok;
  terminal_io.Unix.c_echonl <- status.c_echonl;
  (* Control characters: *)
  terminal_io.Unix.c_vmin <- status.c_vmin;
  terminal_io.Unix.c_vtime <- status.c_vtime;
;;

let set_initial_status, get_initial_status =
  let initial_status = ref None in
  let set x =
    match !initial_status with
    | None -> initial_status := Some x
    | Some _ -> assert false in
  let get () =
    match !initial_status with
    | None -> assert false
    | Some x -> x in
  set, get
;;

let open_term () =
  let terminal_io = Unix.tcgetattr Unix.stdin in
  set_initial_status (to_status terminal_io);
  of_status terminal_io interactive;
  Unix.tcsetattr Unix.stdin Unix.TCSAFLUSH terminal_io
;;

let close_term () =
  let terminal_io = Unix.tcgetattr Unix.stdin in
  of_status terminal_io (get_initial_status ());
  Unix.tcsetattr Unix.stdin Unix.TCSAFLUSH terminal_io
;;
