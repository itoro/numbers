(** Config module. *)

val default_with_term_method : Term.with_term_method
(** [Term.with_term_method] that must be used if user doesn't specify it
    on command line. *)
;;
