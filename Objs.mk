BYTLIBS=\
 unix.cma

BINLIBS=$(BYTLIBS:.cma=.cmxa)

MLIFILES=\
 cube.mli\
 config.mli\
 term_termios.mli\
 term_stty.mli\
 term.mli\
 main.mli

MLFILES=\
 cube.ml\
 config.ml\
 term_termios.ml\
 term_stty.ml\
 term.ml\
 main.ml

CAMLFILES=\
 $(MLIFILES)\
 $(MLFILES)

SPURIOUSFILES=\
 $(MLFILES:.ml=.annot)\
 $(MLFILES:.ml=.o)

INTERFACES=$(MLIFILES:.mli=.cmi)

BYTOBJS=$(MLFILES:.ml=.cmo)

BINOBJS=$(BYTOBJS:.cmo=.cmx)

BYTEXE=numbers.byt

BINEXE=$(BYTEXE:.byt=.bin)

OBJS=\
 $(BYTOBJS)\
 $(BINOBJS)

EXES=\
 $(BYTEXE)\
 $(BINEXE)
