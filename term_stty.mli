(** Module to configure terminal using stty. *)

val open_term : unit -> unit
(** [open_term ()] inits terminal. *)
;;

val close_term : unit -> unit
(** [close_term ()] reset terminal. *)
;;
