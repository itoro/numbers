(** Terminal module. *)

type with_term_method =
  | Stty (** Use stty method *)
  | Termios (** Use termios method *)
(** Terminal method to use. *)
;;

val with_term : with_term_method -> (unit -> 'a) -> 'a
(** [with_term_method term_method f] inits term using [term_method] and
    call [f]. *)
;;

val read_key : unit -> char
(** [read_key ()] waits for a key. *)
;;

val reset : unit -> unit
(** [reset ()] reset terminal and puts cursor to home. *)
;;

val home : unit -> unit
(** [reset ()] puts cursor to home. *)
;;
